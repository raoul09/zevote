<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Zevote</title>

		<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
	</head>
	<body style="margin: auto 200px;">

		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <a class="navbar-brand" href="#">Ze'vote</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
		    <div class="navbar-nav">
		      <a class="nav-item nav-link active" href="{{ route('command') }}">Home <span class="sr-only">(current)</span></a>
		      <a class="nav-item nav-link" href="{{route('hist')}}">Mes courses</a>
		      <a class="nav-item nav-link" href="{{route('list')}}">Leaderboard</a>
		      <a class="nav-item nav-link" href="{{route('admin')}}">Admin</a>
		    </div>
		  </div>
		</nav>
		<div style="padding: 50px; text-align: justify-all;">
		@yield('verify')
		@yield('commande')
		@yield('historique')
		@yield('leaderboard')
		@yield('note')
		@yield('validation')
		</div>
			
		<script type="text/javascript" src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
		<script src="{{ asset('js/popper.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('js/app.js') }}"></script>
	</body>
</html>