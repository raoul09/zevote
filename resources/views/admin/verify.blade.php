@extends('layouts.master')
@section('verify')
	<h3>Liste des commandes à confirmer </h3>

	@foreach($commands as $command)
	@if(empty($command->livreur_id))

		<h4>Commande : {{ $command->lib_commande }}</h4>
		<h5>assigner un livreur</h5>

	<form action="{{ route('post.admin', $command->id) }}" method="post">
		{{method_field('put')}}
		@csrf
		<label for="livreurs">Liste des livreurs disponibles</label>
		<select name="livreur_id" class="form-control">
			@foreach($livreurs as $livreur)
				<option value="{{ $livreur->id }}">{{ $livreur->fullname_liv }}</option>
			@endforeach
			
		</select>
		<input type="submit" class="btn btn-primary">
	</form>
	@else
		<h4>Commande : {{ $command->lib_commande }}</h4>
		<h5 style="color : blue;">Commande Validé</h5>
	@endif
	<hr>
	@endforeach
	{{ $commands->links() }}
@stop
