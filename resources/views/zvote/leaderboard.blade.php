@extends('layouts.master')
@section('leaderboard')
	<h2 style="text-align: center; margin-bottom: 80px;">Classement des livreurs</h2>
	
	<table class="table table-bordered">
		<thead>
			<tr>
				<th style="text-align: center;">Client</th>
				<th style="text-align: center;">Nombre d'avis</th>
				<th style="text-align: center;">Note</th>
			</tr>
		</thead>
		<tbody>
			@foreach($livreurs as $livreur)
				<tr>
					<td><h5>{{ $livreur->fullname_liv }} </h5></td>
			       	<td>{{ $livreur->notes->count() }}</td>
			       	<td><h4>	{{ $livreur->note }}</h4></td>
				</tr>
			@endforeach
		</tbody>
	</table>
@stop
