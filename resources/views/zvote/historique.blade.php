@extends('layouts.master')
@section('historique')
	<h2 style="text-align: center; margin-bottom: 80px;">Historiques</h2>
	
		@foreach($commands as $command)
			<p>
				
				<h5>Commande : {{ $command->lib_commande }}</h5>
				<span>Description : {{ $command->description }}</span>
				@if($command->livreur == true)
					<h5>Livreur :  {{ $command->livreur }} </h5>
				<h5><a href="{{Route('note', $command)}}">Noter ce livreur</a></h5>
				@else
				<h5 style="color: red;">Commande en attente de validation</h5>
				@endif
			</p><hr/>
		@endforeach
	
	{{ $commands->links() }}

	<a href="{{Route('command')}}" class="btn btn-secondary btn-block"><h4>faire une course</h4></a>
@stop