@extends('layouts.master')
@section('note')
	<h3>Livreur : {{ $command->livreur }}</h3>
	<h3>Evaluer la qualité du livreur</h3>
	<form action="{{ Route('post.note', $command) }}" method="post">
		@csrf
		<label for="note"></label>
		<input type="number" name="note" max="5" placeholder="note sur 5" class="form-control form-control-lg"><br>
		<input type="submit" name="submit" value="Noter" class="btn btn-primary btn-block">
	</form>

@stop
