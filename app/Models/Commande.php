<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commande extends Model
{
    protected $fillable = ['fullname_client', 'lib_commande', 'livreur_id', 'localite', 'description', 'numero'];

	public function livreur() 
	{
		return $this->belongsTo('App\Models\Livreur');
	}
}
