<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Livreur extends Model
{
    protected $fillable = ['livreurs','note'];
    public function commandes() 
	{
	    return $this->hasMany('App\Models\Commande');
	}
	public function notes(){
		return $this->hasMany('App\Models\Note');
	}
}
