<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable= ['note', 'fullname_client', 'livreur_id'];
    public function livreur(){
    	return $this->belongsTo('App\Models\Livreur');
    }
}
