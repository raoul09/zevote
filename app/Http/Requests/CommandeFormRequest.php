<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommandeFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lib_commande' =>'required',
            'localite' =>'required|alpha',
            'description' =>'',
            'numero' =>'numeric'
        ];
    }
    public function messages()
    {
       return [
            'required' => 'Ce champ est obligatoire',
            'alpha' => 'veuillez entrer uniquement des lettres'
        ];
        
    }
}
