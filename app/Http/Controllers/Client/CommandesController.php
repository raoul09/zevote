<?php

namespace App\Http\Controllers\Client;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Commande;
use App\Models\Livreur;
use App\Models\Note;
use App\Http\Requests\CommandeFormRequest;

class CommandesController extends Controller
{
    public function command()
    {
    	return view('zvote.commande');
    }
        //commande du client
    public function store(CommandeFormRequest $request)
    {
    	$commands = Commande::create([
    	'lib_commande' => $request->lib_commande,
        'fullname_client' => $request->fullname_client,
        'localite' => $request->localite,
        'description'=>$request->description,
        'numero' => $request->numero
        ]);
    	return redirect()->route('hist');
    }
        //historique des commandes
    public function show()
    {
    	$commands = Commande::where('fullname_client', 'raoul')->orderBy('id', 'desc')->paginate(5);
    	return view('zvote.historique', compact('commands', 'livreurs'));
    }
        //
    public function note($command)
    {
    	$command = Commande::findOrFail($command);
    	return view('zvote.note', compact('command'));
    }
        //attribution de note et enregistrement
    public function post(Request $request, $command)
    {
        $commande = Commande::findOrFail($command);
    	$notes = Note::create([ 'note' => $request->note, 'fullname_client' => $commande->fullname_client,
            'livreur_id' => $commande->livreur_id ]);

        $note_livreur = Note::where('livreur_id', $commande->livreur_id)->avg('note');
        $livreur = Livreur::find($commande->livreur_id);
        $livreur->update([ 'note' => ceil($note_livreur) ]);
    	return redirect(route('list'));
    }
        //liste leaderboard
    public function list()
    {
        $livreurs = Livreur::orderBy('note', 'desc')->get();
        return view('zvote.leaderboard', compact('livreurs'));
    }
}
