<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Commande;
use App\Models\Livreur;

class AdminController extends Controller
{
	public function mgmt(){
		$livreurs = Livreur::all();
		$commands = Commande::orderBy('id', 'desc')->paginate(6);
		return view('admin.verify', compact('commands', 'livreurs'));
	}
    public function update(Request $request, $command)
    {
		$livreur = Livreur::findOrFail(request('livreur_id'));
		$commands = Commande::where('id', $command);
		$commands->update([
			'livreur' => $livreur->fullname_liv,
			'livreur_id'=> request('livreur_id')
		]);
		return back();

	}
}
