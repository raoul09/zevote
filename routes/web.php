<?php


/*Route::get('/', function () {
    return view('welcome');
});*/
Route::namespace('Client')->group(function(){
	
	//Route::post('/post', 'CommandesController@save')->name('post.command');
});
Route::get('/', 'Client\CommandesController@command')->name('command');

Route::post('/', 'Client\CommandesController@store');

Route::get('/hist', 'Client\CommandesController@show')->name('hist');

Route::get('/admin', 'Admin\AdminController@mgmt')->name('admin');
Route::put('/admin/{commande}', 'Admin\AdminController@update')->name('post.admin');

Route::get('/hist/{command}', 'Client\CommandesController@note')->name('note');
Route::post('/hist/{command}', 'Client\CommandesController@post')->name('post.note');

Route::get('/list', 'Client\CommandesController@list')->name('list');
