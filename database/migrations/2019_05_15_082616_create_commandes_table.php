<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commandes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('lib_commande');
            $table->string('fullname_client');
            $table->string('livreur')->nullable();
            $table->integer('livreur_id')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('livreur_id')
                  ->references('id')->on('livreurs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commandes');
    }
}
